CROSS      := arm-linux-gnueabihf
CXX        := $(CROSS)-g++
SUB_DIR    := ./libgpiod
OUT_DIR    := output/usr/local
LIB_FILES  := $(OUT_DIR)/lib/libgpiodcxx.so $(OUT_DIR)/lib/libgpiod.so
TUNE_FLAGS := -march=armv6 -mfpu=vfp -mfloat-abi=hard
WARN_FLAGS := -Wall -Wextra -Wpedantic -Weffc++ -Werror
OPT        := -Og -g
INC        := -isystem $(SUB_DIR)/bindings/cxx -isystem $(SUB_DIR)/include
CXXFLAGS   := $(TUNE_FLAGS) $(WARN_FLAGS) $(INC) $(OPT)

flash: flash.cpp $(LIB_FILES)
	$(CXX) $(CXXFLAGS) $< -o $@ -L $(OUT_DIR)/lib -l gpiodcxx -l gpiod

shift: shift.cpp $(LIB_FILES)
	$(CXX) $(CXXFLAGS) $< -o $@ -L $(OUT_DIR)/lib -l gpiodcxx -l gpiod

$(LIB_FILES):
	mkdir -p $(OUT_DIR)
	cd $(SUB_DIR) && ./autogen.sh --enable-bindings-cxx --host=$(CROSS) --prefix=$$PWD/../$(OUT_DIR) ac_cv_func_malloc_0_nonnull=yes
	make -C $(SUB_DIR)
	make -C $(SUB_DIR) install

.PHONY: clean
clean:
	make -C $(SUB_DIR) clean
	rm -rf $(SUB_DIR)/output flash shift
