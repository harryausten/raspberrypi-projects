#include <gpiod.hpp>
#include <thread>
#include <iostream>

// Time between value changes
std::chrono::milliseconds DELAY { 100U };

// Pi line offsets
const std::uint32_t SER   { 2 };
const std::uint32_t SRCLK { 3 };

// Get chip ref
static gpiod::chip s_chip { "gpiochip0" };

// Get input and clock line refs
static gpiod::line s_input { s_chip.get_line(SER)   };
static gpiod::line s_clock { s_chip.get_line(SRCLK) };

static void shift_bit(bool x)
{
    s_input.set_value(x ? 1 : 0);
    std::this_thread::sleep_for(DELAY);
    s_clock.set_value(1);
    std::this_thread::sleep_for(DELAY);
    s_clock.set_value(0);
    std::this_thread::sleep_for(DELAY);
}

int main(int argc, char ** argv)
{
    // Parse args
    if (argc != 2)
    {
        std::cerr << "Wrong number of arguments!" << std::endl;
        return 1;
    }

    std::uint32_t num { 0 };

    try
    {
        num = std::stoul(argv[1]);
        std::cout << "Inputted number: " << num << std::endl;
    }
    catch ( std::exception & e )
    {
        std::cerr << "Failed converting arg to uint32_t. Received: " << e.what() << std::endl;
        return 1;
    }

    // Set up the config
    gpiod::line_request config {};
    config.consumer = "shift.cpp";
    config.request_type = gpiod::line_request::DIRECTION_OUTPUT;

    // Request the lines (default off)
    s_input.request(config, 0);
    s_clock.request(config, 0);

    for (std::uint8_t i = 0; i < 8; ++i)
    {
        bool bit { static_cast<bool>((num >> i) & 1U) };
        std::cout << "Shifting bit: " << bit << std::endl;
        shift_bit(bit);
    }

    return 0;
}
