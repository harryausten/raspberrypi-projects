#include <gpiod.hpp>
#include <thread>
#include <iostream>

int main(int argc, char ** argv)
{
    // Parse args
    if (argc != 3)
    {
        std::cout << "Wrong number of arguments!" << std::endl;
        return 1;
    }

    unsigned long num_flashes { 0 };
    unsigned long delay_ms { 0 };

    try
    {
        num_flashes = std::stoul(argv[1]);
        delay_ms    = std::stoul(argv[2]);
    }
    catch ( std::exception & e )
    {
        std::cout << "Failed converting args to unsigned ints. Received: " << e.what() << std::endl;
        return 1;
    }

    // Get line ref (0:2)
    auto line { gpiod::chip{"gpiochip0"}.get_line(2) };

    // Set up the config
    gpiod::line_request config {};
    config.consumer = "flash.cpp";
    config.request_type = gpiod::line_request::DIRECTION_OUTPUT;

    // Request the line (default off)
    line.request(config, 0);

    // Time between value changes
    auto delay { std::chrono::milliseconds(delay_ms) };

    for (std::size_t i = 0; i < num_flashes; ++i)
    {
        line.set_value(1);
        std::this_thread::sleep_for(delay);
        line.set_value(0);
        std::this_thread::sleep_for(delay);
    }

    return 0;
}
